#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:27262262:92fc3d29c28a2f5e43257c47c4297c449bc35e69; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25464114:4f597f71a15ee5fd4d1c63bda754c5d238a05bdd EMMC:/dev/block/bootdevice/by-name/recovery 92fc3d29c28a2f5e43257c47c4297c449bc35e69 27262262 4f597f71a15ee5fd4d1c63bda754c5d238a05bdd:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
