#!/system/bin/sh
asuskey_path="/dev/block/bootdevice/by-name/asuskey"
asuskey_sign_path="/data/tmp_asuskey_sign"
log_path="/data/log_unlock"
rm $log_path
echo "[asuskey]starts " >> $log_path 2>&1
if [ -f $asuskey_sign_path ] ; then
	#dd -> 0p22
	echo "[asuskey]will write asuskey " >> $log_path 2>&1
	dd if=$asuskey_sign_path of=$asuskey_path >> $log_path 2>&1
	rm $asuskey_sign_path >> $log_path 2>&1
else
	echo "[asuskey]$asuskey_sign_path not exists" >> $log_path 2>&1
fi
