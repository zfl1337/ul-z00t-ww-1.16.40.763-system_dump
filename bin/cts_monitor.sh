#!/system/bin/sh
LOG_TAG="cts_monitor"
fmrssi=-2

cat /proc/cpuinfo |grep MSM8916 > /dev/null 2>&1 || exit
if [ "$(getprop sys.foregroundapp)" == "com.android.cts.verifier" ];then
	echo 1 > /sys/devices/system/cpu/cpu0/cpuidle/state0/performance_mode
else
	echo 0 > /sys/devices/system/cpu/cpu0/cpuidle/state0/performance_mode
fi
